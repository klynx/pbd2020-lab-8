package si.uni_lj.fri.pbd.lab8.ui.main;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.lab8.Product;
import si.uni_lj.fri.pbd.lab8.ProductRepository;

public class MainViewModel extends AndroidViewModel {

    private LiveData<List<Product>> allProducts;
    private MutableLiveData<List<Product>> searchResults;


    private ProductRepository productRepository;

    public MainViewModel (Application application) {
        super(application);

        productRepository = new ProductRepository(application);
        allProducts = productRepository.getAllProducts();
        searchResults = productRepository.getSearchResults();
    }

    public MutableLiveData<List<Product>> getSearchResults() {
        return this.searchResults;
    }

    public LiveData<List<Product>> getAllProducts() {
        return this.allProducts;
    }

    public void insertProduct(Product product) {
        productRepository.insertProduct(product);
    }

    public void findProduct(String name) {
        productRepository.findProduct(name);
    }

    public void deleteProduct(String name) {
        productRepository.deleteProduct(name);
    }
}
